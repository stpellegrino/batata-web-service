-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 10, 2014 at 12:51 PM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `253project`
--
CREATE DATABASE IF NOT EXISTS `253project` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `253project`;

-- --------------------------------------------------------

--
-- Table structure for table `contract`
--

CREATE TABLE IF NOT EXISTS `contract` (
  `ID` int(11) NOT NULL,
  `Budget` int(11) NOT NULL,
  `Privacy` varchar(100) NOT NULL,
  `Rules` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE IF NOT EXISTS `files` (
  `Extension` int(11) NOT NULL,
  `Media ID` int(11) NOT NULL,
  PRIMARY KEY (`Media ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `has`
--

CREATE TABLE IF NOT EXISTS `has` (
  `User ID` int(11) NOT NULL,
  `Media ID` int(11) NOT NULL,
  PRIMARY KEY (`User ID`,`Media ID`),
  KEY `Media ID` (`Media ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE IF NOT EXISTS `image` (
  `Height` int(11) DEFAULT NULL,
  `Width` int(11) DEFAULT NULL,
  `Resolution` int(11) DEFAULT NULL,
  `URL` varchar(50) NOT NULL,
  `MediaID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`MediaID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `links to all pictures`
--
CREATE TABLE IF NOT EXISTS `links to all pictures` (
`url` varchar(50)
);
-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE IF NOT EXISTS `media` (
  `ID` int(20) NOT NULL AUTO_INCREMENT,
  `Field` varchar(30) DEFAULT NULL,
  `Rating` int(11) DEFAULT NULL,
  `Contract ID` int(11) DEFAULT NULL,
  `URL` varchar(50) DEFAULT NULL,
  `userID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `Contract ID` (`Contract ID`),
  KEY `userID` (`userID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`ID`, `Field`, `Rating`, `Contract ID`, `URL`, `userID`) VALUES
(1, NULL, NULL, NULL, 'google.com', 4),
(2, NULL, NULL, NULL, 'facebook.com', 5);

-- --------------------------------------------------------

--
-- Stand-in structure for view `most recently registered users`
--
CREATE TABLE IF NOT EXISTS `most recently registered users` (
`id` int(11)
,`username` varchar(30)
);
-- --------------------------------------------------------

--
-- Table structure for table `rate`
--

CREATE TABLE IF NOT EXISTS `rate` (
  `Media ID` int(11) NOT NULL,
  `User ID` int(11) NOT NULL,
  PRIMARY KEY (`Media ID`,`User ID`),
  KEY `User ID` (`User ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `recentactions`
--

CREATE TABLE IF NOT EXISTS `recentactions` (
  `ActionID` int(11) NOT NULL,
  `ActionType` varchar(30) NOT NULL,
  `UserID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `recently added images`
--
CREATE TABLE IF NOT EXISTS `recently added images` (
`url` varchar(50)
);
-- --------------------------------------------------------

--
-- Table structure for table `set/recieve`
--

CREATE TABLE IF NOT EXISTS `set/recieve` (
  `User ID` int(11) NOT NULL,
  `Contract ID` int(11) NOT NULL,
  `Acceptance Window` int(11) NOT NULL,
  PRIMARY KEY (`User ID`,`Contract ID`),
  KEY `set/recieve_ibfk_2` (`Contract ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(30) NOT NULL,
  `Username` varchar(30) NOT NULL,
  `Password` varchar(30) DEFAULT NULL,
  `BankAccount` varchar(30) DEFAULT NULL,
  `Rating` int(11) DEFAULT NULL,
  `Field` varchar(30) DEFAULT NULL,
  `ImageURL` varchar(30) DEFAULT NULL,
  `Email` varchar(40) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1235 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`ID`, `Name`, `Username`, `Password`, `BankAccount`, `Rating`, `Field`, `ImageURL`, `Email`) VALUES
(4, 'Tarek', 'tark', NULL, 'test', 5, NULL, NULL, 'asdasdasd@gmail.com'),
(5, 'Tarek', 'tark', NULL, 'test', 0, NULL, NULL, 'asdasdasd@gmail.com'),
(6, 'Tarek', 'tark', NULL, 'test', 0, NULL, NULL, 'asdasdasd@gmail.com'),
(7, 'Tarek', 'tark', NULL, 'test', 0, NULL, NULL, 'asdasdasd@gmail.com'),
(8, 'Tarek', 'tark', NULL, 'test', 0, NULL, NULL, 'asdasdasd@gmail.com'),
(9, 'Tarek', 'tark', NULL, 'test', 0, NULL, NULL, 'asdasdasd@gmail.com'),
(1234, 'Hassan', 'Bayloun', NULL, NULL, NULL, NULL, NULL, 'cute_bayloun@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `userfollowed`
--

CREATE TABLE IF NOT EXISTS `userfollowed` (
  `userID` int(11) NOT NULL,
  `followedID` int(11) NOT NULL,
  KEY `followedID` (`followedID`),
  KEY `userID` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userfollowed`
--

INSERT INTO `userfollowed` (`userID`, `followedID`) VALUES
(5, 4);

-- --------------------------------------------------------

--
-- Table structure for table `userfollowers`
--

CREATE TABLE IF NOT EXISTS `userfollowers` (
  `UserID` int(11) NOT NULL,
  `FollowerID` int(11) NOT NULL,
  KEY `FollowerID_2` (`FollowerID`),
  KEY `UserID` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userfollowers`
--

INSERT INTO `userfollowers` (`UserID`, `FollowerID`) VALUES
(4, 5);

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE IF NOT EXISTS `video` (
  `Resolution` int(11) NOT NULL,
  `Length` int(11) NOT NULL,
  `Media ID` int(11) NOT NULL,
  PRIMARY KEY (`Media ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure for view `links to all pictures`
--
DROP TABLE IF EXISTS `links to all pictures`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `links to all pictures` AS select distinct `media`.`URL` AS `url` from `media`;

-- --------------------------------------------------------

--
-- Structure for view `most recently registered users`
--
DROP TABLE IF EXISTS `most recently registered users`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `most recently registered users` AS select `user`.`ID` AS `id`,`user`.`Username` AS `username` from `user` order by `user`.`ID` desc limit 5;

-- --------------------------------------------------------

--
-- Structure for view `recently added images`
--
DROP TABLE IF EXISTS `recently added images`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `recently added images` AS select `media`.`URL` AS `url` from `media` order by `media`.`ID` desc limit 5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `files`
--
ALTER TABLE `files`
  ADD CONSTRAINT `files_ibfk_1` FOREIGN KEY (`Media ID`) REFERENCES `media` (`ID`);

--
-- Constraints for table `has`
--
ALTER TABLE `has`
  ADD CONSTRAINT `has_ibfk_3` FOREIGN KEY (`User ID`) REFERENCES `user` (`ID`);

--
-- Constraints for table `image`
--
ALTER TABLE `image`
  ADD CONSTRAINT `image_ibfk_1` FOREIGN KEY (`MediaID`) REFERENCES `media` (`ID`) ON DELETE CASCADE;

--
-- Constraints for table `media`
--
ALTER TABLE `media`
  ADD CONSTRAINT `media_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `user` (`ID`) ON DELETE CASCADE;

--
-- Constraints for table `userfollowed`
--
ALTER TABLE `userfollowed`
  ADD CONSTRAINT `userfollowed_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `user` (`ID`),
  ADD CONSTRAINT `userfollowed_ibfk_2` FOREIGN KEY (`followedID`) REFERENCES `user` (`ID`);

--
-- Constraints for table `userfollowers`
--
ALTER TABLE `userfollowers`
  ADD CONSTRAINT `userfollowers_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `user` (`ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `userfollowers_ibfk_2` FOREIGN KEY (`FollowerID`) REFERENCES `user` (`ID`) ON DELETE CASCADE;

--
-- Constraints for table `video`
--
ALTER TABLE `video`
  ADD CONSTRAINT `video_ibfk_1` FOREIGN KEY (`Media ID`) REFERENCES `media` (`ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
