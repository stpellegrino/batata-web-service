-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 17, 2014 at 12:02 PM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `batata`
--
CREATE DATABASE IF NOT EXISTS `batata` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `batata`;

-- --------------------------------------------------------

--
-- Table structure for table `contract`
--

CREATE TABLE IF NOT EXISTS `contract` (
  `ID` int(11) NOT NULL,
  `Budget` int(11) NOT NULL,
  `Privacy` varchar(100) NOT NULL,
  `Rules` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contract`
--

INSERT INTO `contract` (`ID`, `Budget`, `Privacy`, `Rules`) VALUES
(1, 2, 'three', 'four'),
(11, 15, 'system', 'hello'),
(2013, 23123, 'test one two three', 'test one two three'),
(201302, 123, 'test haha', 'test 2 hahsdha');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE IF NOT EXISTS `files` (
  `Extension` int(11) NOT NULL,
  `Media ID` int(11) NOT NULL,
  PRIMARY KEY (`Media ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`Extension`, `Media ID`) VALUES
(123, 230123),
(12312356, 12312312);

-- --------------------------------------------------------

--
-- Table structure for table `has`
--

CREATE TABLE IF NOT EXISTS `has` (
  `User ID` int(11) NOT NULL,
  `Media ID` int(11) NOT NULL,
  PRIMARY KEY (`User ID`,`Media ID`),
  KEY `Media ID` (`Media ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE IF NOT EXISTS `image` (
  `Height` int(11) NOT NULL,
  `Width` int(11) NOT NULL,
  `Resolution` int(11) NOT NULL,
  `Media ID` int(11) NOT NULL,
  PRIMARY KEY (`Media ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE IF NOT EXISTS `media` (
  `ID` int(11) NOT NULL,
  `Field` varchar(30) NOT NULL,
  `Rating` int(11) NOT NULL,
  `Contract ID` int(11) NOT NULL,
  `URL` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Contract ID` (`Contract ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`ID`, `Field`, `Rating`, `Contract ID`, `URL`) VALUES
(230123, 'testone', 2, 2013, NULL),
(12312312, 'asdasd', 123, 2013, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rate`
--

CREATE TABLE IF NOT EXISTS `rate` (
  `Media ID` int(11) NOT NULL,
  `User ID` int(11) NOT NULL,
  PRIMARY KEY (`Media ID`,`User ID`),
  KEY `User ID` (`User ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rate/follow`
--

CREATE TABLE IF NOT EXISTS `rate/follow` (
  `User_ID` int(11) NOT NULL,
  `User2_ID` int(11) NOT NULL,
  PRIMARY KEY (`User_ID`,`User2_ID`),
  KEY `rate/follow_ibfk_2` (`User2_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `set/recieve`
--

CREATE TABLE IF NOT EXISTS `set/recieve` (
  `User ID` int(11) NOT NULL,
  `Contract ID` int(11) NOT NULL,
  `Acceptance Window` int(11) NOT NULL,
  PRIMARY KEY (`User ID`,`Contract ID`),
  KEY `set/recieve_ibfk_2` (`Contract ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `ID` int(11) NOT NULL,
  `Name` varchar(30) NOT NULL,
  `Username` varchar(30) NOT NULL,
  `Password` varchar(30) DEFAULT NULL,
  `BankAccount` varchar(30) DEFAULT NULL,
  `Rating` int(11) DEFAULT NULL,
  `Field` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`ID`, `Name`, `Username`, `Password`, `BankAccount`, `Rating`, `Field`) VALUES
(0, 'Tarek', 'tarekjamo', NULL, 'test', 0, NULL),
(1234, 'hassan', 'hbayloun', NULL, NULL, NULL, NULL),
(12345, 'two', 'three', 'four', 'five', 6, 'seven'),
(123235, 'hasasdasd', 'asdasdasd', 'asd', NULL, NULL, NULL),
(123452, 'two', 'three', 'four', 'five', 6, 'seven'),
(201302764, 'Hassan Bayloun', 'cute_bayloun', 'test', 'Lebanese bank', 10, 'test');

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE IF NOT EXISTS `video` (
  `Resolution` int(11) NOT NULL,
  `Length` int(11) NOT NULL,
  `Media ID` int(11) NOT NULL,
  PRIMARY KEY (`Media ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `files`
--
ALTER TABLE `files`
  ADD CONSTRAINT `files_ibfk_1` FOREIGN KEY (`Media ID`) REFERENCES `media` (`ID`);

--
-- Constraints for table `has`
--
ALTER TABLE `has`
  ADD CONSTRAINT `has_ibfk_2` FOREIGN KEY (`Media ID`) REFERENCES `media` (`ID`),
  ADD CONSTRAINT `has_ibfk_1` FOREIGN KEY (`User ID`) REFERENCES `user` (`ID`);

--
-- Constraints for table `image`
--
ALTER TABLE `image`
  ADD CONSTRAINT `image_ibfk_1` FOREIGN KEY (`Media ID`) REFERENCES `media` (`ID`);

--
-- Constraints for table `media`
--
ALTER TABLE `media`
  ADD CONSTRAINT `media_ibfk_1` FOREIGN KEY (`Contract ID`) REFERENCES `contract` (`ID`);

--
-- Constraints for table `rate`
--
ALTER TABLE `rate`
  ADD CONSTRAINT `rate_ibfk_2` FOREIGN KEY (`User ID`) REFERENCES `user` (`ID`),
  ADD CONSTRAINT `rate_ibfk_1` FOREIGN KEY (`Media ID`) REFERENCES `media` (`ID`);

--
-- Constraints for table `video`
--
ALTER TABLE `video`
  ADD CONSTRAINT `video_ibfk_1` FOREIGN KEY (`Media ID`) REFERENCES `media` (`ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
