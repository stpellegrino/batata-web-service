package showee2;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.ws.soap.MTOM;

@MTOM

@WebService(endpointInterface = "showee2.FileTransfer", portName = "FileTransferPort", serviceName = "FileTransfer")


@SOAPBinding(style = SOAPBinding.Style.RPC)


public class FileTransferImp implements FileTransfer {
@Override
public String upload(byte[] data , String fileName) {
String filePath = "data/" + fileName ;
try {
	
	System.out.println("Connected");
	InputStream in=null;
	
FileOutputStream fos = new FileOutputStream(filePath);
BufferedOutputStream outputStream = new BufferedOutputStream(fos);
outputStream.write(data);
outputStream.flush();
outputStream.close();
return "true" ;
} catch (IOException ex) {
return ex.getMessage() ;
}
}
}
