package showee2;

import java.sql.DriverManager;
import java.sql.SQLException;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

@WebService(endpointInterface = "showee2.Database", portName = "DatabasePort", serviceName = "Database")


@SOAPBinding(style = SOAPBinding.Style.RPC)




public class DatabaseImp implements Database {
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String MYSQL_DB_HOST = System.getenv("OPENSHIFT_MYSQL_DB_HOST");
	static final String MYSQL_DB_PORT = System.getenv("OPENSHIFT_MYSQL_DB_PORT");
	static final String DB_URL = "jdbc:mysql://"+MYSQL_DB_HOST+":"+MYSQL_DB_PORT+ "/showee2";
	// /the name of the database
	static final String USER = "adminaucLAmq"; // Database credentials
	static final String PASS = "gnHJHYnYJXJL"; // Database credentials
	
	
	public String write ( int id , int x, int y, String name ) {
		Connection conn = null ;
		Statement stmt = null ;
		try {
			Class.forName ( JDBC_DRIVER ); // Register JDBC driver
			conn = (Connection) DriverManager.getConnection( DB_URL , USER , PASS ); // Open a connection
			stmt = (Statement) conn.createStatement (); // Execute a query
			String sql = " INSERT INTO Transaction VALUES (" + id + "," + x + "," + y + ", '" + name + "' )";
			stmt.executeUpdate ( sql );
			return "Inserted records into the table ... ";
		} catch ( Exception e) {
			return e. getMessage ();
		} finally {
			try {
				if ( stmt != null ) conn.close ();
			} catch ( SQLException e) {
				return e. getMessage ();
			}
		}
		}
}
