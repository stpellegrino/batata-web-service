package showee2;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.soap.MTOM;

@MTOM

@WebService(endpointInterface = "showee2.FileDownload", portName = "FileDownloadPort", serviceName = "FileDownload")


@SOAPBinding(style = SOAPBinding.Style.RPC)


public class FileDownloadImp implements FileDownload {
@Override
public byte[] download(String location, String fileName) {
	String filePath = "data/" + fileName;
    System.out.println("Sending file: " + filePath);
     
    try {
        File file = new File(filePath);
        FileInputStream fis = new FileInputStream(file);
        BufferedInputStream inputStream = new BufferedInputStream(fis);
        byte[] fileBytes = new byte[(int) file.length()];
        inputStream.read(fileBytes);
        inputStream.close();
         
        return fileBytes;
    } catch (IOException ex) {
        System.err.println(ex);
        throw new WebServiceException(ex);
    }
}
}
