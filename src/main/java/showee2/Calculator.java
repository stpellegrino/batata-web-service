package showee2;

import javax.jws.WebMethod ;
import javax.jws.WebService ;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style=Style.RPC)
public interface Calculator {
	@WebMethod public int add(int x, int y) ;
	@WebMethod public int multiply(int x, int y) ;
}


