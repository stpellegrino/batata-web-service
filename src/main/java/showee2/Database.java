package showee2;

import javax.jws.WebMethod ;
import javax.jws.WebService; 
import javax.jws.soap.SOAPBinding ;
import javax.jws.soap.SOAPBinding.Style; 

@WebService
@SOAPBinding(style = Style.RPC)
public interface Database {
 @WebMethod String write(int id, int x, int y, String name) ;
}
