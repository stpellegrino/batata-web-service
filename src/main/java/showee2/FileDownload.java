package showee2;


import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.xml.ws.soap.MTOM;

@MTOM
@WebService
@SOAPBinding(style=Style.RPC)
public interface FileDownload{
	@WebMethod public byte[] download(String dir, String fileName);
	
	}
