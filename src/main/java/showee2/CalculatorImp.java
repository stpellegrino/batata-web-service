package showee2;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService(endpointInterface = "showee2.Calculator", portName = "CalculatorPort", serviceName = "Calculator")


@SOAPBinding(style = SOAPBinding.Style.RPC)







public class CalculatorImp implements Calculator {
	public int add(int x, int y) {
		return x + y;
	}

	public int multiply(int x, int y) {
		return x * y;
	}
}
