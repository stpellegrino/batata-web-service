package sqlInterface;

import engine.User;

import java.sql.*;

public class sql {
	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static final String DB_URL = "jdbc:mysql://localhost/253project";

	//  Database credentials
	static final String USER = "root";
	static final String PASS = "";

	public static void main(String[] args) {
		System.out.println(login("tark", "test"));
	}

	public static void addMail(String username, String email) {
		//Add the following url to the image of the user
				Connection conn = null;
				Statement stmt = null;
				try{	
					Class.forName("com.mysql.jdbc.Driver");

					System.out.println("Connecting to a selected database...");
					conn = DriverManager.getConnection(DB_URL, USER, PASS);
					System.out.println("Connected database successfully...");

					System.out.println("Inserting records into the table...");

					String updateSQL = "UPDATE UsEr "
							+"SET Email='"+email+"' "
							+"WHERE Username='"+username+"'";
					System.out.println(updateSQL);
					PreparedStatement preparedStatement = conn.prepareStatement(updateSQL);
					preparedStatement.executeUpdate();
					System.out.println("Inserted records into the table...");

				}catch(SQLException se){
					//Handle errors for JDBC
					se.printStackTrace();
				}catch(Exception e){
					//Handle errors for Class.forName
					e.printStackTrace();
				}finally{
					//finally block used to close resources
					try{
						if(stmt!=null)
							conn.close();
					}catch(SQLException se){
					}// do nothing
					try{
						if(conn!=null)
							conn.close();
					}catch(SQLException se){
						se.printStackTrace();
					}//end finally try
				}//end try
				System.out.println("Goodbye!");
	}
	public static boolean login(String username, String password) {
		Connection conn = null;
		Statement stmt = null;
		boolean login = false;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("Connected database successfully...");
			System.out.println("Inserting records into the table...");
			
			String insertTableSQL = "SELECT username FROM `user` WHERE username='"+username+"'";
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(insertTableSQL);
			if(rs.next()) {
				String insertTableSQL2 = "SELECT password FROM `user` WHERE password='"+password+"'";
				stmt = conn.createStatement();
				ResultSet rs2 = stmt.executeQuery(insertTableSQL2);
				if(rs2.next()) {
					login = true;
				}
			}
			System.out.println("Inserted records into the table...");
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					conn.close();
			}catch(SQLException se){
			}// do nothing
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}//end try
		System.out.println("Goodbye!");
		return login;
	} 

	public static void addUser(User u) {
		Connection conn = null;
		Statement stmt = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");

			System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("Connected database successfully...");
			
			System.out.println("Inserting records into the table...");

			String insertTableSQL = "INSERT INTO User"
					+ "(ID, Name, Username, Password, BankAccount, Rating, Field) VALUES"
					+ "(?,?,?,?,?,?,?)";
			PreparedStatement preparedStatement = conn.prepareStatement(insertTableSQL);
			preparedStatement.setInt(1, u.getId());
			preparedStatement.setString(2, u.getName());
			preparedStatement.setString(3, u.getUsername());
			preparedStatement.setString(4, u.getPassword());
			preparedStatement.setString(5, u.getBankAccount());
			preparedStatement.setInt(6, u.getOverallRate());
			preparedStatement.setString(7, u.getField());
			preparedStatement .executeUpdate();
			System.out.println("Inserted records into the table...");

		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					conn.close();
			}catch(SQLException se){
			}// do nothing
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}//end try
		System.out.println("Goodbye!");
	} 
}
