package sqlInterface;



import engine.User;
import java.sql.*;

public class sqlLocal {
	// JDBC driver name and database URLsdsd
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static final String DB_URL = "jdbc:mysql://localhost/batata";

	//  Database credentials
	static final String USER = "root";
	static final String PASS = "";

	public static void main(String[] args) {
		User test = new User("Tarek", "Jamo", "tarekjamo", "tarekjamo@gmail.com");
		addUser(test);
		addImage(test, "hello");
	}
	
	public static void addMail(String username, String email)a {
		//Add the following url to the image of the user
				Connection conn = null;
				Statement stmt = null;
				try{
					Class.forName("com.mysql.jdbc.Driver");

					System.out.println("Connecting to a selected database...");
					conn = DriverManager.getConnection(DB_URL, USER, PASS);
					System.out.println("Connected database successfully...");

					System.out.println("Inserting records into the table...");

					String updateSQL = "UPDATE User "
							+"SET Email="+email+" "
							+"WHERE Username="+username;
					System.out.println(updateSQL);
					PreparedStatement preparedStatement = conn.prepareStatement(updateSQL);
					preparedStatement.executeUpdate();
					System.out.println("Inserted records into the table...");

				}catch(SQLException se){
					//Handle errors for JDBC
					se.printStackTrace();
				}catch(Exception e){
					//Handle errors for Class.forName
					e.printStackTrace();
				}finally{
					//finally block used to close resources
					try{
						if(stmt!=null)
							conn.close();
					}catch(SQLException se){
					}// do nothing
					try{
						if(conn!=null)
							conn.close();
					}catch(SQLException se){
						se.printStackTrace();
					}//end finally try
				}//end try
[]				System.out.println("Goodbye!");
	}

	public static void login(String username, String password) {
		Connection conn = null;
		Statement stmt = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");

			System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("Connected database successfully...");

			System.out.println("Inserting records into the table...");

			int i=2;

			String userNameExists = "SELECT * FROM user WHERE EXISTS ("+
					"SELECT * FROM user WHERE user.ID =12345)";
			System.out.println(userNameExists);
			PreparedStatement preparedStatement = conn.prepareStatement(userNameExists);
			preparedStatement.executeUpdate();
			System.out.println("Inserted records into the table...");

		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					conn.close();
			}catch(SQLException se){
			}// do nothing
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}//end try
		System.out.println("Goodbye!");
	} 
	
	public static void addImage(User u, String url) {
		//Add the following url to the image of the user
		Connection conn = null;
		Statement stmt = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");

			System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("Connected database successfully...");

			System.out.println("Inserting records into the table...");

			int i=2;

			String updateSQL = "UPDATE User "
					+"SET ImageURL='google.com' "
					+"WHERE ID="+u.getId();
			System.out.println(updateSQL);
			PreparedStatement preparedStatement = conn.prepareStatement(updateSQL);
			preparedStatement.executeUpdate();
			System.out.println("Inserted records into the table...");

		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					conn.close();
			}catch(SQLException se){
			}// do nothing
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}//end try
		System.out.println("Goodbye!");
	} 

	public static void addUser(User u) {
		Connection conn = null;
		Statement stmt = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");

			System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("Connected database successfully...");

			System.out.println("Inserting records into the table...");

			int i=1;

			String insertTableSQL = "INSERT INTO User"
					+ "(ID, Name, Username, Password, BankAccount, Rating, Field, ImageURL) VALUES"
					+ "(?,?,?,?,?,?,?,?)";
			PreparedStatement preparedStatement = conn.prepareStatement(insertTableSQL);
			preparedStatement.setInt(1, u.getId()+i);
			preparedStatement.setString(2, u.getName());
			preparedStatement.setString(3, u.getUsername());
			preparedStatement.setString(4, u.getPassword());
			preparedStatement.setString(5, u.getBankAccount());
			preparedStatement.setInt(6, u.getOverallRate());
			preparedStatement.setString(7, u.getField());
			preparedStatement.setInt(8, i);
			i++;
			preparedStatement.executeUpdate();
			System.out.println("Inserted records into the table...");

		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					conn.close();
			}catch(SQLException se){
			}// do nothing
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}//end try
		System.out.println("Goodbye!");
	} 
}
