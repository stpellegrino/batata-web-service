package sqlInterface;

import engine.User;

import java.sql.*;

public class Sql2 {
	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static final String DB_URL = "jdbc:mysql://localhost/253project";

	//  Database credentials
	static final String USER = "root";
	static final String PASS = "";

	public static void main(String[] args) {
		deleteImage(5);
	}
	
	public static void deleteImage(int id) {
		Connection conn = null;
		Statement stmt = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");

			System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("Connected database successfully...");

			System.out.println("Inserting records into the table...");

			
			String insertTableSQL = "DELETE FROM Media WHERE id="+id;
			PreparedStatement preparedStatement = conn.prepareStatement(insertTableSQL);
			preparedStatement.executeUpdate();
			
			String insertTableSQL2 = "DELETE FROM Image WHERE mediaid="+id;
			PreparedStatement preparedStatement2 = conn.prepareStatement(insertTableSQL2);
			preparedStatement2.executeUpdate();
			System.out.println("Inserted records into the table...");
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					conn.close();
			}catch(SQLException se){
			}// do nothing
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}//end try
		System.out.println("Goodbye!");
	}
	
	//please note this will also delete associated media and followers
	public static void deleteUser(int id) {
		Connection conn = null;
		Statement stmt = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");

			System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("Connected database successfully...");

			System.out.println("Inserting records into the table...");

			
			String insertTableSQL = "DELETE FROM user WHERE id="+id;
			PreparedStatement preparedStatement = conn.prepareStatement(insertTableSQL);
			preparedStatement.executeUpdate();
			System.out.println("Inserted records into the table...");
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					conn.close();
			}catch(SQLException se){
			}// do nothing
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}//end try
		System.out.println("Goodbye!");
	}

	public static int getID(String username) {
		Connection conn = null;
		Statement stmt = null;
		int result = -1;
		try{
			Class.forName("com.mysql.jdbc.Driver");

			System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("Connected database successfully...");

			System.out.println("Inserting records into the table...");

			
			String insertTableSQL = "SELECT id FROM `user` WHERE Username='"+username+"'";
			stmt = (Statement) conn.createStatement();

			stmt.execute(insertTableSQL);
			ResultSet rs = stmt.getResultSet();

			if(rs.next()) {
				result = rs.getInt(1) ;
			}

			System.out.println("Inserted records into the table...");
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					conn.close();
			}catch(SQLException se){
			}// do nothing
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}//end try
		System.out.println("Goodbye!");
		return result;
	}


	//returns the media ids of all the images linked to one user(that they have uploaded)
	public static int[] getImages(int userID) {
		Connection conn = null;
		Statement stmt = null;
		int[] result = new int[10];
		try{
			Class.forName("com.mysql.jdbc.Driver");

			System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("Connected database successfully...");

			System.out.println("Inserting records into the table...");

			String insertTableSQL = "SELECT ID FROM `media` WHERE UserID="+userID;
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(insertTableSQL);
			int i=0;
			while(rs.next()) {
				result[i] = rs.getInt(1);
				i++;
			}
			System.out.println("Inserted records into the table...");
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					conn.close();
			}catch(SQLException se){
			}// do nothing
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}//end try
		System.out.println("Goodbye!");
		return result;
	}

	public static String getImage(int userID) {
		Connection conn = null;
		Statement stmt = null;
		String result = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");

			System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("Connected database successfully...");

			System.out.println("Inserting records into the table...");

			String insertTableSQL = "SELECT ImageURL FROM `user` WHERE id="+userID;
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(insertTableSQL);
			if(rs.next()) {
				result = rs.getString(1);
			}
			System.out.println("Inserted records into the table...");
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					conn.close();
			}catch(SQLException se){
			}// do nothing
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}//end try
		System.out.println("Goodbye!");
		return result;
	}

	public static void addImage(int userID, String url) {
		Connection conn = null;
		Statement stmt = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");

			System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("Connected database successfully...");

			System.out.println("Inserting records into the table...");

			String insertTableSQL = "INSERT INTO media"
					+ "(UserID, URL) VALUES"
					+ "(?,?)";
			PreparedStatement preparedStatement = conn.prepareStatement(insertTableSQL);
			preparedStatement.setInt(1, userID);
			preparedStatement.setString(2, url);
			preparedStatement.executeUpdate();

			String insertTableSQL3 = "SELECT ID FROM `media` WHERE url='"+url+"'";
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(insertTableSQL3);
			int result = 0;
			if(rs.next()) {
				result = rs.getInt(1);
			}

			String insertTableSQL2 = "INSERT INTO Image"
					+ "(MediaID, URL) VALUES"
					+ "(?,?)";
			PreparedStatement preparedStatement2 = conn.prepareStatement(insertTableSQL2);
			preparedStatement2.setInt(1, result);
			preparedStatement2.setString(2, url);
			preparedStatement2.executeUpdate();
			System.out.println("Inserted records into the table...");
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					conn.close();
			}catch(SQLException se){
			}// do nothing
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}//end try
		System.out.println("Goodbye!");
	}

	public static String getUsername(int id) {
		Connection conn = null;
		Statement stmt = null;
		String result = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");

			System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("Connected database successfully...");

			System.out.println("Inserting records into the table...");

			String insertTableSQL = "SELECT username FROM `user` WHERE id="+id;
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(insertTableSQL);
			if(rs.next()) {
				result = rs.getString(1);
			}
			System.out.println("Inserted records into the table...");
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					conn.close();
			}catch(SQLException se){
			}// do nothing
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}//end try
		System.out.println("Goodbye!");
		return result;
	}

	public static int[] listOfFollowers(int userID) {
		Connection conn = null;
		Statement stmt = null;
		int[] result = new int[10];
		try{
			Class.forName("com.mysql.jdbc.Driver");

			System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("Connected database successfully...");

			System.out.println("Inserting records into the table...");

			String insertTableSQL = "SELECT FollowerID FROM `userFollowers` WHERE UserID="+userID;
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(insertTableSQL);
			int i=0;
			while(rs.next()) {
				result[i] = rs.getInt(1);
				i++;
			}
			System.out.println("Inserted records into the table...");
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					conn.close();
			}catch(SQLException se){
			}// do nothing
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}//end try
		System.out.println("Goodbye!");
		return result;
	}

	public static void followthisUser(int user, int follower) {
		//follower follows user
		Connection conn = null;
		Statement stmt = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");

			System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("Connected database successfully...");

			System.out.println("Inserting records into the table...");

			String insertTableSQL = "INSERT INTO UserFollowers"
					+ "(UserID, FollowerID) VALUES"
					+ "(?,?)";
			PreparedStatement preparedStatement = conn.prepareStatement(insertTableSQL);
			preparedStatement.setInt(1, user);
			preparedStatement.setInt(2, follower);
			preparedStatement.executeUpdate();
			System.out.println("Inserted records into the table...");
			userhasFollowed(follower, user);
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					conn.close();
			}catch(SQLException se){
			}// do nothing
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}//end try
		System.out.println("Goodbye!");
	}

	public static void userhasFollowed(int user, int follower) {
		//user has followed these followers
		Connection conn = null;
		Statement stmt = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");

			System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("Connected database successfully...");

			System.out.println("Inserting records into the table...");

			String insertTableSQL = "INSERT INTO UserFollowed"
					+ "(UserID, FollowedID) VALUES"
					+ "(?,?)";
			PreparedStatement preparedStatement = conn.prepareStatement(insertTableSQL);
			preparedStatement.setInt(1, user);
			preparedStatement.setInt(2, follower);
			preparedStatement.executeUpdate();
			System.out.println("Inserted records into the table...");

		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					conn.close();
			}catch(SQLException se){
			}// do nothing
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}//end try
		System.out.println("Goodbye!");
	}
}