package sqlInterface;

import engine.User;

import java.sql.*;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

public class sqlImp implements sql {
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String MYSQL_DB_HOST = System.getenv("OPENSHIFT_MYSQL_DB_HOST");
	static final String MYSQL_DB_PORT = System.getenv("OPENSHIFT_MYSQL_DB_PORT");
	static final String DB_URL = "jdbc:mysql://"+MYSQL_DB_HOST+":"+MYSQL_DB_PORT+ "/showee2";
	// /the name of the database
	static final String USER = "adminaucLAmq"; // Database credentials
	static final String PASS = "gnHJHYnYJXJL"; // Database credentials
	

	
	public Connection connect() {
		Connection conn = null ;
		Statement stmt = null ;
		try {
			Class.forName ( JDBC_DRIVER ); // Register JDBC driver
			conn = (Connection) DriverManager.getConnection( DB_URL , USER , PASS ); // Open a connection
			stmt = (Statement) conn.createStatement (); // Execute a query
			
			return conn ;
			//return "Modified database...";
		} catch ( Exception e) {
			//return e. getMessage ();
			e.printStackTrace();
		} finally {
			try {
				if ( stmt != null ) conn.close ();
			} catch ( SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public void addImage(User u, String url) {
		//Add the following url to the image of the user


	}

	public void addUser(User u) {
		Connection conn = null;
		Statement stmt = null;
		try{
			

			String insertTableSQL = "INSERT INTO User"
					+ "(ID, Name, Username, Password, BankAccount, Rating, Field) VALUES"
					+ "(?,?,?,?,?,?,?)";
			conn = connect() ;
			PreparedStatement preparedStatement = conn.prepareStatement(insertTableSQL);
			preparedStatement.setInt(1, u.getId());
			preparedStatement.setString(2, u.getName());
			preparedStatement.setString(3, u.getUsername());
			preparedStatement.setString(4, u.getPassword());
			preparedStatement.setString(5, u.getBankAccount());
			preparedStatement.setInt(6, u.getOverallRate());
			preparedStatement.setString(7, u.getField());
			preparedStatement.executeUpdate();
			System.out.println("Inserted records into the table...");

		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					conn.close();
			}catch(SQLException se){
			}// do nothing
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}//end try
		System.out.println("Goodbye!");
	} 
}
