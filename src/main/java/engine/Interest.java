package engine;

public class Interest implements InterestInterface
{
	private Image[] images; // array of images that define the interest 
	private String name; // name of the intrest
	private User[] lovers; // people that have this interest 
	public Interest(String name, Image[] im)
	{ 
		this.name=name;
		this.images=im ; 
	}
	public Interest(String name)
	{ 
		this.name=name; 
	}
}
