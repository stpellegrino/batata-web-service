package engine;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

//interface made to compute the average rating and all the reviews of the user friom the followers
@WebService

public interface ImageInterface {
	@WebMethod void followedBy(User u) ;
	
		
	 


	@WebMethod int getId() ;
	

	@WebMethod void setId(int id) ;
	

	@WebMethod byte[] getData() ;
		

	@WebMethod void setData(byte[] data) ;
	
}
