package engine;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style=Style.RPC)
public interface BanchAccountInterface {
	
	
	
	@WebMethod String getBankName() ; 
	
		@WebMethod void setBankName(String bankName)  ;
	
}
