package engine;

import java.awt.Image;
import java.util.Date;

import javax.sound.midi.SoundbankResource;


public class Contract implements interactionInterface,  ContractInterface
{

	private User[] users; // the users that are in the contract 
	private String regulations; // the regelations and rules of the project
	private User Lawyer; // every contract has a lawyer that makes sure that every thing is fine
	private Transaction[] transactions; // the transactions of the contract 
	private Image[] images; // images of the project
	private Date Deadline; // deadline of the project
	private rating external; // rating of outsiders, they give their opinion 
	private rating official; // rating of the users in the project 
	
	
	 
	/*
	 * This method checks if the user is in the contract,
	 * if he is: adds a rating to the official rating  
	 * if he is not: adds a rating to the external rating 
	 * @see interaction#ratedBy(User, rating)
	 */
	@Override
	public void ratedBy(User u, rating r)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void followedBy(User u) {
		// TODO Auto-generated method stub
		
	}

}
