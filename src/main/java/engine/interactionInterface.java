package engine;
//interface that allows users and contracts to  get rated 

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style=Style.RPC)
public interface interactionInterface 
{
	@WebMethod  public void ratedBy(User u, rating r); // methods that lets a user rate another, if the user wants to only rate he can with review=null, or he can write one too!  
	@WebMethod  public void followedBy(User u); 
}
