package engine;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.Date;

import showee2.FileTransfer; 
import showee2.FileTransferImp;
import sqlInterface.sql;
import sqlInterface.sqlImp;

@WebService(endpointInterface = "showee2.User", portName = "UserPort", serviceName = "User")


@SOAPBinding(style = SOAPBinding.Style.RPC)



public class User implements UserInterface, ComputeRatingInterface, interactionInterface
{
	private static int id; // the unique id of the user 
	private String name; //the name 
	private String last; // the last name
	private String username; // the username
	private String password; // the password 
	private String field; // the field 
	private Date dob; // the date of birth of the user 
	private String email; //email
	private Interest [] interests; // the list of interests of the user 
	private BankAccount BankAccount; // the Bank account to be use in the app
	private rating[] rating; // array containing the total written reviews of the user with the rating of each
	private int overallRate; // the average rating of the user 
	private User[] followers; // the list of followers 
	private int numbFol; // the number of followers 
	private User[] following; // the list of people the user is following
	private Contract[] previousContracts;// list of all the contracts that user worked on 

	//const with minimum requirements 

	public boolean registerNewUser() {
		// This method will be called when adding the user to the database for the 1st time.
		sqlImp sqlimp = new sqlImp() ;
		sqlimp.addUser(this) ;
		return false;
		
	}
	public User(String name, String last, String username, String email)
	{ 
		this.id=id++; 
		this.setName(name);
		this.last=last;
		this.setUsername(username); 
		this.setEmail(email); 
	}
	//method that sets the bank account of the user 
	public void setBankAcct(BankAccount b)
	{ 
		this.setBankAccount(b); 
	}
	//method that sets dob
	public void setDob(Date d)

	{ 
		this.dob=d;
	}
	/*
	 * modify: changes the average rating of this user
	 * add: add the rating to the array of ratings of the user 
	 * takes: the user that rated this one and the rating that the user defined  
	 * @see interaction#ratedBy(User, rating)
	 */
	@Override
	public void ratedBy(User u, rating r) 
	{
		// TODO Auto-generated method stub

	}
	/*
	 * add: add the user to the followers array, increments the number of followers 
	 * takes: the user that followed this.user
	 * 
	 * @see interaction#followedBy(User)
	 */
	@Override
	public void followedBy(User u)
	{
		// TODO Auto-generated method stub

	}
	/* 
	 * method that creates a request and sends it to another user
	 * dk how to implement this yet
	 */
	public void createRequest(User u) 
	{ 

	}

	/*
	 *  method that computes the overAllRate
	 *  takes all the ratings.rate from rating and computes the average! 
	 */
	public int computeAvgRating() 
	{
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	public void addImage(Image im)  {
		// add Image 
		FileTransferImp a = new FileTransferImp() ;
		a.upload(im.getData(), this.id+""+im.getId()+"") ;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getOverallRate() {
		return overallRate;
	}
	public int getId() {
		return id;
	}
	public void setOverallRate(int overallRate) {
		this.overallRate = overallRate;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getBankAccount() {
		return "test"; //placeholder
	}
	public void setBankAccount(BankAccount bankAccount) {
		BankAccount = bankAccount;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	
}
