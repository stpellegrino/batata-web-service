package engine;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

//interface made to compute the average rating and all the reviews of the user friom the followers
@WebService
@SOAPBinding(style=Style.RPC)
public interface ComputeRatingInterface
{
	@WebMethod public int computeAvgRating(); 
}
