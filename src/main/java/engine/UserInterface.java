package engine;

import java.util.Date;

import javax.jws.WebMethod ;
import javax.jws.WebService ;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style=Style.RPC)
public interface UserInterface { // we need a user interface for the web service server technical side :)
	@WebMethod boolean registerNewUser();
	@WebMethod void setBankAcct(BankAccount b);
	@WebMethod void ratedBy(User u, rating r)  ;
	@WebMethod void setDob(Date d) ;
	@WebMethod void followedBy(User u) ;
	@WebMethod void createRequest(User u)  ;
	@WebMethod int computeAvgRating()  ;
	@WebMethod void addImage(Image im)  ;
	@WebMethod String getName() ;
	@WebMethod 	void setName(String name) ;
	@WebMethod String getUsername()  ;
	@WebMethod void setUsername(String username) ;
	@WebMethod String getEmail() ;
	@WebMethod void setEmail(String email);
	@WebMethod int getOverallRate() ;
	@WebMethod int getId() ; 
	@WebMethod void setOverallRate(int overallRate) ; 
	@WebMethod String getPassword() ; 
	@WebMethod void setPassword(String password) ;
	@WebMethod String getBankAccount() ; 
	@WebMethod void setBankAccount(BankAccount bankAccount) ; 
	@WebMethod String getField()  ; 
	@WebMethod void setField(String field) ;
}
