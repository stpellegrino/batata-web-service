package engine;


public class Transaction implements TransactionInterface 
{
	private User payer;
	private User receiver;
	private double amount;
	
	/*
	 *  constructor assigns the details to save the transitions
	 *  modifies: adds the amount to the balance of the receiver and removes it from the payer balance
	 */
	
	public Transaction(User u1, User u2, double amount)
	{
		this.amount=amount;
		this.payer=u1;
		this.receiver=u2;
		
	}
	
	
}
