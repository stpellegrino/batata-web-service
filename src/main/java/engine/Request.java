package engine;

import java.awt.Image;

/* 
 * a request is the offer that a user sends to another user who can accept of refuse.
 * if the user accepts than a new contract is formed 
 */
public class Request implements RequestInterface 
{
	private User sender; // sender
	private User receiver;  // receiver 
	private boolean accepted; //acceptance of the receiver
	private String details; //the details of the project 
	private Image[] images; 
	public Request(User sender, User receiver, Image[] images, String details)
	{ 
		this.sender=sender; 
		this.receiver=receiver;
		this.images=images; 
		this.details=details; 
		
	}
	//gets accpted by receiver 
	public boolean getsAccepted()
	{ 
		accepted=true;
		return accepted; 
	}
	
}
