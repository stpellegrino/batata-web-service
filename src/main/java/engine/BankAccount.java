package engine;

public class BankAccount implements BanchAccountInterface 
{
	private double balance; // the amount 
	private String BankName; // the bank
	private String CreditCard; // the credit card code
	public BankAccount(double balance, String bank)
	{ 
		this.balance=balance; 
		this.setBankName(bank); 
	}
	public String getBankName() {
		return BankName;
	}
	public void setBankName(String bankName) {
		BankName = bankName;
	}
	
}
