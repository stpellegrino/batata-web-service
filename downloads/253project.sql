-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 26, 2014 at 11:32 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `253project`
--
CREATE DATABASE IF NOT EXISTS `253project` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `253project`;

-- --------------------------------------------------------

--
-- Table structure for table `contract`
--

CREATE TABLE IF NOT EXISTS `contract` (
  `ID` int(11) NOT NULL,
  `Budget` int(11) NOT NULL,
  `Privacy` varchar(100) NOT NULL,
  `Rules` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE IF NOT EXISTS `files` (
  `Extension` int(11) NOT NULL,
  `Media ID` int(11) NOT NULL,
  PRIMARY KEY (`Media ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`Extension`, `Media ID`) VALUES
(123, 230123),
(12312356, 12312312);

-- --------------------------------------------------------

--
-- Table structure for table `has`
--

CREATE TABLE IF NOT EXISTS `has` (
  `User ID` int(11) NOT NULL,
  `Media ID` int(11) NOT NULL,
  PRIMARY KEY (`User ID`,`Media ID`),
  KEY `Media ID` (`Media ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE IF NOT EXISTS `image` (
  `Height` int(11) NOT NULL,
  `Width` int(11) NOT NULL,
  `Resolution` int(11) NOT NULL,
  `URL` varchar(50) NOT NULL,
  `Media ID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  PRIMARY KEY (`Media ID`),
  KEY `UserID` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE IF NOT EXISTS `media` (
  `ID` int(11) NOT NULL,
  `Field` varchar(30) NOT NULL,
  `Rating` int(11) NOT NULL,
  `Contract ID` int(11) NOT NULL,
  `URL` varchar(50) DEFAULT NULL,
  `userID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `Contract ID` (`Contract ID`),
  KEY `userID` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`ID`, `Field`, `Rating`, `Contract ID`, `URL`, `userID`) VALUES
(230123, 'testone', 2, 2013, NULL, 0),
(12312312, 'asdasd', 123, 2013, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `rate`
--

CREATE TABLE IF NOT EXISTS `rate` (
  `Media ID` int(11) NOT NULL,
  `User ID` int(11) NOT NULL,
  PRIMARY KEY (`Media ID`,`User ID`),
  KEY `User ID` (`User ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rate/follow`
--

CREATE TABLE IF NOT EXISTS `rate/follow` (
  `User_ID` int(11) NOT NULL,
  `User2_ID` int(11) NOT NULL,
  PRIMARY KEY (`User_ID`,`User2_ID`),
  KEY `rate/follow_ibfk_2` (`User2_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `set/recieve`
--

CREATE TABLE IF NOT EXISTS `set/recieve` (
  `User ID` int(11) NOT NULL,
  `Contract ID` int(11) NOT NULL,
  `Acceptance Window` int(11) NOT NULL,
  PRIMARY KEY (`User ID`,`Contract ID`),
  KEY `set/recieve_ibfk_2` (`Contract ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(30) NOT NULL,
  `Username` varchar(30) NOT NULL,
  `Password` varchar(30) DEFAULT NULL,
  `BankAccount` varchar(30) DEFAULT NULL,
  `Rating` int(11) DEFAULT NULL,
  `Field` varchar(30) DEFAULT NULL,
  `ImageURL` varchar(30) DEFAULT NULL,
  `Email` varchar(40) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`ID`, `Name`, `Username`, `Password`, `BankAccount`, `Rating`, `Field`, `ImageURL`, `Email`) VALUES
(3, 'Tarek', 'tark', NULL, 'test', 0, NULL, NULL, 'test@gmail.com'),
(4, 'Tarek', 'tark', NULL, 'test', 0, NULL, NULL, 'test@gmail.com'),
(5, 'Tarek', 'tark', NULL, 'test', 0, NULL, NULL, 'test@gmail.com'),
(6, 'Tarek', 'tark', NULL, 'test', 0, NULL, NULL, 'test@gmail.com'),
(7, 'Tarek', 'tark', NULL, 'test', 0, NULL, NULL, 'test@gmail.com'),
(8, 'Tarek', 'tark', NULL, 'test', 0, NULL, NULL, 'test@gmail.com'),
(9, 'Tarek', 'tark', NULL, 'test', 0, NULL, NULL, 'test@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE IF NOT EXISTS `video` (
  `Resolution` int(11) NOT NULL,
  `Length` int(11) NOT NULL,
  `Media ID` int(11) NOT NULL,
  PRIMARY KEY (`Media ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `files`
--
ALTER TABLE `files`
  ADD CONSTRAINT `files_ibfk_1` FOREIGN KEY (`Media ID`) REFERENCES `media` (`ID`);

--
-- Constraints for table `has`
--
ALTER TABLE `has`
  ADD CONSTRAINT `has_ibfk_3` FOREIGN KEY (`User ID`) REFERENCES `user` (`ID`),
  ADD CONSTRAINT `has_ibfk_2` FOREIGN KEY (`Media ID`) REFERENCES `media` (`ID`);

--
-- Constraints for table `image`
--
ALTER TABLE `image`
  ADD CONSTRAINT `image_ibfk_1` FOREIGN KEY (`Media ID`) REFERENCES `media` (`ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
